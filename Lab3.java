package day1;

public class Lab3 {
	public static void main(String[] args) {
		int i = 20;
		i++;
		i++;
		i++;
		i++;
		i++;
		System.out.println(i);
		i--;
		i--;
		i--;
		i--;
		i--;
		System.out.println(i);
		conditionOp();

	}

	public static void conditionOp() {
		float fnum = 1.5f;
		float fnum1 = 1.6f;
		if (fnum == 1.5f && fnum1 == 1.6f) {
			System.out.println("fnum1 = 1.5f AND fnum2 = 1.6f");
		}
		if (fnum == 1.5f || fnum1 == 1.5f) {
			System.out.println("fnum1 = 1.5f OR fnum2 = 1.5f");
		}
	}

}
