package day1;

public class Lab6 {
	public static void main(String[] args) {
		int i = 1;
		while (i <= 10) {
			System.out.println("i :" + i);
			i++;
		}
		int i2 = 1;
		int sum = 0;
		while (i2 <= 10) {
			sum = sum + i2;
			i2++;
		}
		System.out.println("Total :" + sum);

		int count = 1;
		while (count <= 100) {
			if (count % 12 == 0) {
				System.out.println("Count %12 = 0 is :" + count);
			}
			count++;
		}

		int[] array = { 1, 2, 3, 4, 5, 6 };
		for (int counter : array) {
			System.out.println("counter :" + counter);
		}

	}

}
