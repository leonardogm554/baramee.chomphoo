package day1;

public class Lab2 {
	public static void main(String[] args) {
		bark();
		primitive();
	}

	public static void bark() {
		String dogName = "Doge";
		System.out.println("The Dog name " + dogName + "bark");
	}

	public static void primitive() {
		// float -> int
		float fnum = 1.55f;
		int inum = (int) fnum;
		System.out.println(inum);
		// int -> float
		int inum2 = 1;
		float fnum2 = inum2;
		System.out.println(fnum2);
		// double -> float
		float fnum3 = 1.5f;
		double dnum = fnum3;
		System.out.println(dnum);
		// char -> int
		char cchar = '1';
		int inum3 = cchar;
		System.out.println(inum3);
	}
	// final variable can not change value

}
