package day1;

public class Lab1 {
	public static void main(String[] args) {
		lab1();
		lab1optional();
	}

	public static void lab1() {
		int number = 1;
		boolean toggle = true;
		String name = "Stang";

		System.out.println("This is a int: " + number);
		System.out.println("This is a boolean: " + toggle);
		System.out.println("This is a String: " + name);
	}

	public static void lab1optional() {
		double miles = 100;
		final double MILES_TO_KILOMETER = 1.609;
		double kilometer = miles * MILES_TO_KILOMETER;
		System.out.println("Answer is " + kilometer + " kilometer");

	}

}
