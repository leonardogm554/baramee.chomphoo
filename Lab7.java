package day1;

import java.util.Scanner;

public class Lab7 {
	public static void main(String[] args) {
		// 1
		int number = 20;
		do {
			
			System.out.println("Number : "+number);
			number--;
		} while(number > 0);
		
		
		// 2
		Scanner scan = new Scanner(System.in);
		
		int equal;
		do {
			System.out.println("Input : ");
			int number2 = scan.nextInt();
			System.out.println("Number is : "+number2);
			equal = number2%2;
		} while (equal == 0);
	}
}
