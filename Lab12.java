package day1;

public class Lab12 {
	public static void main(String[] args) {
		String String1 = "You and Me";
		String String2 = "you and me";
		//1
		if(String1.equals(String2)) {
			System.out.println("Sentence are equal");
		}else {
			System.out.println("Sentence are not equal");
		}
		//2
		if(String1.contains("You")){
			System.out.println("You");
		}else {
			System.out.println("Not contain 'You'");
		}
		
		//3
		System.out.println("String1's length is : "+ (String1.length()));
		System.out.println("String2's length is : "+ (String2.length()));
		//4
		System.out.println("Answer1 is : "+ (String1.substring(1,4)));
		System.out.println("Answer2 is : "+ (String2.substring(1,4)));
		//5
		System.out.println("String1 after trim is : "+(String1.trim()));
		System.out.println("String2 after trim is : "+(String2.trim()));
		//6
		System.out.println("String 1 uppercase is : "+(String1.toUpperCase()));
		System.out.println("String 2 uppercase is : "+(String2.toUpperCase()));
		//7
		System.out.println("String2 is : "+(String2.toUpperCase().trim()));
	}
}
